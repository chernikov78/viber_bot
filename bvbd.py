#!/usr/bin/env python3

from daemon import Daemon
import sys
from sessions import Sessions

import json
import requests
import re
import urllib
import time

import socket

from flask import Flask, request, Response
from gevent.pywsgi import WSGIServer
from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration

from viberbot.api.viber_requests import ViberConversationStartedRequest
from viberbot.api.viber_requests import ViberFailedRequest
from viberbot.api.viber_requests import ViberMessageRequest
from viberbot.api.viber_requests import ViberSubscribedRequest
from viberbot.api.viber_requests import ViberUnsubscribedRequest

from viberbot.api.messages import (
    TextMessage,
    ContactMessage,
    PictureMessage,
    KeyboardMessage,
    VideoMessage,
    LocationMessage
)
from viberbot.api.messages.data_types.contact import Contact
from viberbot.api.messages.data_types.location import Location

from db_sql import DbProvider

#import pdb

IP_ADDR_SRV  = 'IPserver'
PORT_SRV     = 5051
CERT_FILE    ='/etc/letsencrypt/live/botest.top/fullchain.pem'
KEY_FILE     ='/etc/letsencrypt/live/botest.top/privkey.pem'
WEBHOOK_ADDR = 'https://botest.top:' + str(PORT_SRV) + '/'
PID_FILE     = '/var/run/daemons/bot-ots-viber.pid'
API_URL      = 'API'

BKG_BUTTOM_COLOR = "#0404b4"
TEXT_COLOR       = '"#ffffff"'
TEXT_SIZE        = 'large'

PROP_LOG    = '/home/cel/viber_bot/proposition.txt'
WIZARD_LOG  = '/home/cel/viber_bot/wizard.txt'
LOG_FILE    = '/home/cel/viber_bot/log.txt'
MY_ID       = 'ID'

BASE        ='vbots'
TABLE       ='viber'
USR_PWD     ='usr@pwd'

CHAT_ID_TELEGRAM = 'ID'
TOKEN_API    = 'token'
TEL_API_URL  = 'https://api.telegram.org/bot'+TOKEN_API+'/'

bot_configuration = BotConfiguration(
	name='OfficeTechServiceBot',
	avatar='https://ots.com.ua/storage/app/media/logo_ots.png',
	auth_token='token'
)

viber = Api(bot_configuration)
app = Flask(__name__)
session = Sessions('/home/cel/viber_bot/sessions/')
dbprovider = DbProvider(BASE,TABLE,USR_PWD)

@app.route('/', methods=['POST'])
def incoming():
	#logger.debug("received request. post data: {0}".format(request.get_data()))
	# handle the request here

    if not viber.verify_signature(request.get_data(), request.headers.get('X-Viber-Content-Signature')):
        return Response(status=403)

    # this library supplies a simple way to receive a request object
    viber_request = viber.parse_request(request.get_data())

    if isinstance(viber_request, ViberMessageRequest):
        message = viber_request.message
        with open(LOG_FILE, 'a') as log:
            log_record = {'text': message.text, 'name':viber_request.sender.name, 'id': viber_request.sender.id, 'time':time.asctime()}
            log.write(str(log_record)+',\n')
        dic={'user_id':viber_request.sender.id, 'user_name':viber_request.sender.name, 'user_subscr':'0'}
        dbprovider.write_to_db(dic)
        analyse_message(request_id=viber_request.sender.id, message=message, sender_name=viber_request.sender.name)
    elif isinstance(viber_request, ViberConversationStartedRequest):
        day = the_part_of_day()
        viber.send_messages(viber_request.user.id, [TextMessage(text=day + " :)\nЯ бот компанії 'ОфісТехСервіс'\nНапишіть мені 'Привіт', і я вам підкажу!")])
    elif isinstance(viber_request, ViberFailedRequest):
        #logger.warn("client failed receiving message. failure: {0}".format(viber_request))
        pass
    return Response(status=200)

def send_message_telegram(message_text=''):
    url = TEL_API_URL + 'sendMessage'
    answer = {'chat_id':CHAT_ID_TELEGRAM, 'text':message_text}
    r = requests.post(url, json=answer)
    return r



def analyse_message(request_id, message, sender_name=''):
    res =session.read_session(request_id)
    session.write_session(request_id, 'NONE')
    if res =='SET_PROPOSITION':
        txt = 'Пропозиція від ' + sender_name + '\n'+message.text
        with open(PROP_LOG, 'a') as log:
            log_record = {'text': message.text, 'name':sender_name, 'id': request_id, 'time':time.asctime()}
            log.write(str(log_record)+',\n')
        mess = TextMessage(tracking_data={}, text=txt)
        viber.send_messages(MY_ID, mess)
        mess = TextMessage(tracking_data={}, text='Дякую! :)\nЯ передам вашу думку керівництву.')
        viber.send_messages(request_id, mess)
        is_next(request_id)
        return 'Success'
    if res =='SET_CALL_ORDER':
        txt = 'Виклик від ' + sender_name + '\n'+message.text
        with open(WIZARD_LOG, 'a') as log:
            log_record = {'text': message.text, 'name':sender_name, 'id': request_id, 'time':time.asctime()}
            log.write(str(log_record)+',\n')
        mess = TextMessage(tracking_data={}, text=txt)
        send_message_telegram(txt)
        viber.send_messages(MY_ID, mess)
        mess = TextMessage(tracking_data={}, text="Дякую! :)\nЯ вже передав все нашим співробітникам. Найближчим часом ми з вами зв'яжимось.")
        viber.send_messages(request_id, mess)
        is_next(request_id)
        return 'Success'
    txt = message.text.upper()
    if res =='GET_ORDER_INFO':
        tx = txt.replace('K', 'К').replace('B', 'В')
        rr = re.search(r'\bКВ-\d{7}', tx)
        if rr:
            query= API_URL+'/'+urllib.parse.quote(rr[0])+'/123'
            resp = requests.get(query)
            if resp.status_code == 200:
                resp_text = resp.json()['status']
            else:
                resp_text = 'Сталася помилка під час обробки запиту. Спробуйте знову.'
            mess = TextMessage(tracking_data={}, text = resp_text)
            viber.send_messages(request_id, mess) 
        else:
            mess = TextMessage(tracking_data={}, text = "Введене вами значення не є номером квитанції :(\nФормат номера квитанції 'КВ-0000000'")
            viber.send_messages(request_id, mess)
        is_next(request_id)
        return 'Success'
    if txt=='GET_CONTACTS':
        contact = Contact(name="3-44-07", phone_number="+380474434407")
        contact_message = ContactMessage(contact=contact)
        viber.send_messages(request_id, contact_message)
        contact = Contact(name="3-98-74", phone_number="+380474439874")
        contact_message = ContactMessage(contact=contact)
        viber.send_messages(request_id, contact_message)
        contact = Contact(name="(067)473-62-69", phone_number="+380674736269")
        contact_message = ContactMessage(contact=contact)
        viber.send_messages(request_id, contact_message)
        is_next(request_id)
    elif txt == 'GET_WORKS_TIME':
        mess = TextMessage(tracking_data={}, text = 'Пн-Пт      9:00-17:00\nСб            10:00-14:00\nНеділя     Вихідний\n\nА я відповідаю на питання 24/7! :)')
        viber.send_messages(request_id, mess) 
        is_next(request_id)
    elif txt == 'GET_ORDER_INFO':
        session.write_session(request_id, 'GET_ORDER_INFO')
        mess = TextMessage(tracking_data={}, text = 'Введіть номер квитанції')
        viber.send_messages(request_id, mess)
    elif txt == 'GET_MAP':
        location = Location(lat=48.758630, lon=30.205919)
        location_message = LocationMessage(location=location)
        viber.send_messages(request_id, location_message)
        is_next(request_id)
    elif txt == 'SET_YES':
       show_menu(request_id, 'Ok. Чим можу допомогти? :)')
    elif txt == 'SET_NO':
        mess = TextMessage(tracking_data={}, text = 'Радий був допомогти. :)\nЯкщо щось знадобиться - просто привітайтесь зі мною, або посміхніться. :)')
        viber.send_messages(request_id, mess)
    elif txt == 'SET_PROPOSITION':
        session.write_session(request_id, 'SET_PROPOSITION')
        mess = TextMessage(tracking_data={}, text = "Напишіть будь ласка свою пропозицію, і ми її обов'язково проаналізуємо. :)")
        viber.send_messages(request_id, mess)
    elif txt == 'SET_CALL_ORDER':
        session.write_session(request_id, 'SET_CALL_ORDER')
        mess = TextMessage(tracking_data={}, text = "Опишить будь ласка свою проблему та залиште номер телефона для зв'язку. :)")
        viber.send_messages(request_id, mess)
    elif txt == 'MAKE_ORDER':
        show_menu_order(request_id, 'Оберіть будь ласка тип виклика.')
    elif txt == 'REFILL_CARTRIDGES':
        mstxt = 'У нас є картриджі на заправку! Заберіть будь-ласка!'
        txt = 'Виклик від ' + sender_name + '\n'+mstxt
        with open(WIZARD_LOG, 'a') as log:
            log_record = {'text': mstxt, 'name':sender_name, 'id': request_id, 'time':time.asctime()}
            log.write(str(log_record)+',\n')
        mess = TextMessage(tracking_data={}, text=txt)
        send_message_telegram(txt)
        viber.send_messages(MY_ID, mess)
        mess = TextMessage(tracking_data={}, text="Дякую! :)\nЯ вже передав все нашим співробітникам. Найближчим часом ми з вами зв'яжимось.")
        viber.send_messages(request_id, mess)
        is_next(request_id)
        return 'Success'
    elif txt=='(SMILEY)':
        show_menu(request_id, 'Привіт! Я тут. :)\nЧим можу бути корисним?')
    else:
        rr = re.search(r'\bПРИВІТ', txt)
        if rr:
            resp_text = the_part_of_day()
            show_menu(request_id, resp_text + '\nЧим можу допомогти?')
        else:
            mess = TextMessage(tracking_data={}, text = "Напишіть мені 'Привіт', або просто посміхніться, і я вам підкажу. :)")
            viber.send_messages(request_id, mess) 
    return 'Success'

def the_part_of_day():
    time_struct = time.localtime(time.time())
    hour = time_struct.tm_hour
    if hour < 4:
        result = 'Доброї ночі!'
    elif hour <=10:
        result = 'Доброго ранку!'
    elif hour <=17:
        result = 'Доброго дня!'
    elif hour <=22:
        result = 'Доброго вечора!'
    else:
        result = 'Доброї ночі!'
    return result

def is_next(request_id):
    keyboard = {"DefaultHeight": True,
                                "BgColor": "#FFFFFF",
                                "Type": "keyboard",
                                "Buttons": [{
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "SET_YES",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Так</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "SET_NO",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Ні</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            }]
                                }
    mess = TextMessage(tracking_data={}, keyboard=keyboard, min_api_version=4, text = 'У Вас ще є запитання?')
    viber.send_messages(request_id, mess)
    return

def show_menu(request_id, text = ''):
    keyboard = {"DefaultHeight": True,
                                "BgColor": "#FFFFFF",
                                "Type": "keyboard",
                                "Buttons": [{
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor":BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "GET_CONTACTS",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Контакти</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "GET_WORKS_TIME",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Графік роботи</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "GET_ORDER_INFO",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Перебіг ремонту</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "GET_MAP",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Карта</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "SET_PROPOSITION",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Побажання, пропозиції та відгуки</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "MAKE_ORDER",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Виклик майстра</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            }]
                }
    if text:
        mess = TextMessage(tracking_data={}, keyboard=keyboard, text = text)
    else:
        mess = KeyboardMessage(tracking_data={}, min_api_version=4, keyboard = keyboard)
    viber.send_messages(request_id, mess)
    return

def show_menu_order(request_id, text = ''):
    keyboard = {"DefaultHeight": True,
                                "BgColor": "#FFFFFF",
                                "Type": "keyboard",
                                "Buttons": [{
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor":BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "REFILL_CARTRIDGES",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Є картриджі на заправку</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            },
                                            {
                                            "Columns": 3,
                                            "Rows": 1,
                                            "BgColor": BKG_BUTTOM_COLOR,
                                            "ActionType": "reply",
                                            "ActionBody": "SET_CALL_ORDER",
                                            "Text": "<font color="+TEXT_COLOR+"><b>Інше</b></font>",
                                            "TextVAlign": "middle",
                                            "TextHAlign": "center",
                                            "TextOpacity": 100,
                                            "TextSize": TEXT_SIZE
                                            }]
                }
    if text:
        mess = TextMessage(tracking_data={}, keyboard=keyboard, text = text)
    else:
        mess = KeyboardMessage(tracking_data={}, min_api_version=4, keyboard = keyboard)
    viber.send_messages(request_id, mess)
    return

class MyDaemon(Daemon):
    def run(self):
        http_server = WSGIServer((IP_ADDR_SRV, PORT_SRV), app, certfile=CERT_FILE, keyfile=KEY_FILE)
        http_server.serve_forever()

if __name__ == '__main__':
    myDaemon = MyDaemon(PID_FILE)
    if len(sys.argv) ==2:
        if 'start'==sys.argv[1]:
            myDaemon.start()
        elif 'stop'==sys.argv[1]:
            myDaemon.stop()
        elif 'restart'==sys.argv[1]:
            myDaemon.restart()
        else:
            print("Unknown command")
            print("usage: %s start|stop|restart" % sys.argv[0])
            sys.exit(2)
        sys.exit(0)
    else:
        print("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)