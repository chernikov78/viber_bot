import sqlalchemy
#import pdb

class DbProvider:

	def __init__(self, base, table, user):
		self.conn_str = 'mysql://'+user+'@localhost/'+base+'?charset=utf8'
		self.table = table
		self.base = base
		self.create_table()

	def create_table(self):
		query = "SHOW TABLES FROM %(yb)s LIKE '%(yt)s'"%{'yt':self.table, 'yb':self.base}
		cur = self.make_select(query)
		rows = cur.fetchall()
		cur.close()
		if not len(rows):
			query = "CREATE TABLE %(yt)s (user_id VARCHAR(50) NOT NULL PRIMARY KEY, user_name VARCHAR(50), user_subscr BOOLEAN default 0) ENGINE=INNODB"%{'yt':self.table}
			cur = self.make_select(query)
			cur.close()

	def make_select(self, sql):	
		engine = sqlalchemy.create_engine(self.conn_str)
		conn = engine.connect()
		cur=conn.execute(sql)
		conn.close()
		return cur

	#dict{user_id, user_name, user_subscr}
	def write_to_db(self, dict):
		query="SELECT user_id FROM %(yt)s WHERE user_id='%(uid)s'"%{'yt':self.table, 'uid': dict['user_id']}
		cur = self.make_select(query)
		rows = cur.fetchall()
		cur.close()
		if len(rows):
			pass
		else:
			query="INSERT INTO viber (user_id, user_name, user_subscr) VALUES('%(id)s','%(fn)s',%(sb)s)"%{'fn':dict.get('user_name'), 'sb':dict.get('user_subscr','0'),'id':dict['user_id']}
			cur = self.make_select(query)
			cur.close()